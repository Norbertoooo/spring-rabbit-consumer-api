package com.vitu.spring.rabbit.consumer.consumer;

import com.vitu.spring.rabbit.consumer.domain.Pessoa;
import com.vitu.spring.rabbit.consumer.repository.PessoaRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class PessoaConsumer {

    private final PessoaRepository pessoaRepository;

    @RabbitListener(queues = "${queue.pessoa.name}")
    public void consume(Pessoa pessoa) {
        log.info("{}", pessoa);
        Pessoa save = pessoaRepository.save(pessoa);
        log.info("{}", save);
    }

}
