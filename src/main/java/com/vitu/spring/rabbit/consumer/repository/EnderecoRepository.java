package com.vitu.spring.rabbit.consumer.repository;

import com.vitu.spring.rabbit.consumer.domain.Endereco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, Long> {
}
