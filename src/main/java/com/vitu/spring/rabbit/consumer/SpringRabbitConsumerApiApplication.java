package com.vitu.spring.rabbit.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringRabbitConsumerApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringRabbitConsumerApiApplication.class, args);
	}

}
