package com.vitu.spring.rabbit.consumer.controller;

import com.vitu.spring.rabbit.consumer.domain.Pessoa;
import com.vitu.spring.rabbit.consumer.repository.PessoaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/pessoa")
@RequiredArgsConstructor
public class PessoaResource {

    private final PessoaRepository pessoaRepository;

    @GetMapping("/{rua}")
    public List<Pessoa> findPessoaByRua(@PathVariable String rua) {
        return pessoaRepository.findByEndereco_Rua(rua);
    }

    @GetMapping("/not/{nome}")
    public List<Pessoa> findPessoaIsNotRua(@PathVariable String nome) {
        return pessoaRepository.findByNomeIsNot(nome);
    }

}
