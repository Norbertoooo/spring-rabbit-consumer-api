package com.vitu.spring.rabbit.consumer.repository;

import com.vitu.spring.rabbit.consumer.domain.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

    List<Pessoa> findByEndereco_Rua(String rua);

    List<Pessoa> findByNomeIsNot(String nome);

}
