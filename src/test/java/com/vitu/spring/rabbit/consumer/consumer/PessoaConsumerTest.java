package com.vitu.spring.rabbit.consumer.consumer;


import com.vitu.spring.rabbit.consumer.domain.Pessoa;
import com.vitu.spring.rabbit.consumer.repository.PessoaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.vitu.spring.rabbit.consumer.AbstractTest.createPessoa;
import static org.mockito.Mockito.verify;

@ExtendWith(SpringExtension.class)
public class PessoaConsumerTest {

    @Mock
    PessoaRepository pessoaRepository;

    PessoaConsumer pessoaConsumer;

    @BeforeEach
    void setUp() {
        pessoaConsumer = new PessoaConsumer(pessoaRepository);
    }

    @Test
    void ConsumeTest() throws InterruptedException {

        Pessoa pessoa = createPessoa();

        pessoaConsumer.consume(pessoa);

        verify(pessoaRepository, Mockito.times(1)).save(pessoa);

    }

}
