package com.vitu.spring.rabbit.consumer;

import com.vitu.spring.rabbit.consumer.domain.Endereco;
import com.vitu.spring.rabbit.consumer.domain.Pessoa;

public abstract class AbstractTest {

    public static Pessoa createPessoa() {
        return Pessoa.builder()
                .cpf("12332112321")
                .endereco(Endereco.builder().numero(12L).rua("rua").build())
                .idade(12)
                .nome("vitu")
                .build();
    }
}
